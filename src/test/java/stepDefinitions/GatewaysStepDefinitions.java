package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.en.*;
import functions.BasePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;


import java.util.List;
import java.util.concurrent.TimeUnit;

public class GatewaysStepDefinitions extends BasePage {
    WebDriver driver;

    /******** Log Attribute ********/
    Logger log = Logger.getLogger(StepDefinitions.class);


    public GatewaysStepDefinitions() throws Exception {
        driver = Hooks.driver;
        readPage("LoginPage.json");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    /******** Scenario Attributes ********/
    Scenario scenario = null;
    public void scenario (Scenario scenario) {
        this.scenario = scenario;
    }


    @When("^Selecciono organización (.+)$")
    public void selOrganization(String organization) throws Exception {
        selectOrganization(organization);
        waitForElementPresent("Home");

    }

    @And("^Voy a lista de gateways$")
    public void navigateGateways() throws Exception {
        readPage("GatewaysPage.json");
        clickOnElement("Equipos de comunicación");
        clickOnElement("Lista de gateways");
        //waitForElementPresent("Validate gateway creation");
    }

    @Then("^Obtengo lista de gateways creados$")
    public void getGateways() throws Exception {
        List<String> lista = getTextElements("Traer todos los gateways");
        log.info("************************************[ Lista de elementos encontrados ]*****************************************");
        log.info(lista);
        log.info("***************************************************************************************************************");
    }

    @And("^Encuentro el gateway: (.+) en la lista de gateways$")
    public void searchGateway(String gateway) throws Exception {
        List<String> lista = getTextElements("Traer todos los gateways");
        boolean textIsThere = lista.contains(gateway);
        if(textIsThere){
            log.info("The gateway: <"+ gateway +"> is on the list: " + " PASSED");
        }else{
            throw new Error("The gateway <"+ gateway +"> is not on the list: FAILED");
        }

    }
}
