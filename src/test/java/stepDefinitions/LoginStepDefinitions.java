package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.en.*;
import cucumber.api.java.es.Y;
import functions.BasePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

public class LoginStepDefinitions extends BasePage {

    /******** Attributes ********/
    WebDriver driver;
    Logger log = Logger.getLogger(StepDefinitions.class);
    //private static Properties prop = new Properties();
    //private static InputStream in = CreateDriver.class.getResourceAsStream("../test.properties");

    /******** Contructor ********/
    public LoginStepDefinitions() throws Exception {
        driver = Hooks.driver;
        readPage("LoginPage.json");
    }

    Scenario scenario = null;
    public void scenario (Scenario scenario) {
        this.scenario = scenario;
    }


    @Given("^Ingreso a la página principal de Azimut$")
    public void navigateAzimut() throws Exception {
        String url = readProperties("UrlProd");
        //String url = prop.getProperty("UrlProd");
        log.info("Navigate to: " + url);
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //functions.HandleMyWindows.put("Principal", driver.getWindowHandle());
    }

    @When("^Ingreso nombre de usuario válido$")
    public void enterUsername() throws Exception {
        sendText("Username","lmass");

    }

    @And("^Ingreso contraseña correcta$")
    public void enterPassword() throws Exception {
        sendText("Password","Mass_2021#");
    }

    @And("^Doy click en entrar$")
    public void clickButtonLogIn() throws Exception {
        By SeleniumElement = BasePage.getCompleteElement("Iniciar sesion");
        driver.findElement(SeleniumElement).click();
        log.info("Click on: Iniciar sesion");
    }

    @Then("^Ingreso a lista de organizaciones y selecciono (.*)$")
    public void validateOrganizations(String organization) throws InterruptedException {
        Thread.sleep(5000);
    }

    @Y("^cambio de organización a: (.*)")
    public void changeOrganization(String organization) throws Exception {
        readPage("HomePage.json");
        clickOnElement("Icono cerrar sesión");
        clickOnElement("Cambiar organización");
        //readPage("LoginPage.json");
        selectOrganization(organization);
    }

}

