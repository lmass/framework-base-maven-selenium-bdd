package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import cucumber.api.java.es.Y;
import functions.BasePage;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class HistoryAlarmsSCADAStepDefinitions extends BasePage {

    /******** Attributes ********/
    WebDriver driver;
    Logger log = Logger.getLogger(StepDefinitions.class);

    /******** Contructor ********/
    public HistoryAlarmsSCADAStepDefinitions() throws Exception {
        driver = Hooks.driver;
        readPage("HomePage.json");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    Scenario scenario = null;
    public void scenario (Scenario scenario) {
        this.scenario = scenario;
    }

    @Entonces("^Tengo la opción en el panel lateral de ir a histórico de eventos de las alarmas$")
    public void panelAlarmHistory() throws Exception {
        clickOnElement("SCADA > Alarmas");
        Assert.assertEquals("Histórico alarmas", getTextElement("SCADA > Alarmas > Histórico alarmas"));
    }

    @Cuando("^doy click a Histórico Alarmas$")
    public void clickOnHistoryAlarm() throws Exception {
        clickOnElement("SCADA > Alarmas");
        clickOnElement("SCADA > Alarmas > Histórico alarmas");
        readPage("HistoryAlarmsSCADAPage.json");
        waitForElementVisible("Fecha");
    }

    @Entonces("^espero que direccione la página al histórico de alarmas con la siguiente información: Fecha$")
    public void redirectHistoryALarm() throws Exception {
        Assert.assertEquals("Fecha", getTextElement("Fecha"));
    }

    @Y("^Fecha cierre$")
    public void dateClose() throws Exception {
        Assert.assertEquals("Fecha cierre", getTextElement("Fecha cierre"));
    }

    @Y("^Acción$")
    public void action() throws Exception {
        Assert.assertEquals("Acción", getTextElement("Acción"));
    }

    @Y("^Nombre alarma$")
    public void alarmName() throws Exception {
        Assert.assertEquals("Nombre", getTextElement("Nombre"));
    }

    @Y("^Prioridad$")
    public void priority() throws Exception {
        Assert.assertEquals("Prioridad", getTextElement("Prioridad"));
    }

    @Y("^Inmueble$")
    public void headquarter() throws Exception {
        Assert.assertEquals("Inmueble", getTextElement("Inmueble"));
    }

    @Y("^Tiempo para resolverlo$")
    public void timeToSolve() throws Exception {
        Assert.assertEquals("Tiempo para resolver", getTextElement("Tiempo para resolver"));
    }

    @Y("^Tiempo en el que se resolvió$")
    public void timeResolved() throws Exception {
        Assert.assertEquals("Tiempo en el que se resolvió", getTextElement("Tiempo en el que se resolvió"));
    }

    @Y("^Se cumplió el tiempo$")
    public void timeIsUp() throws Exception {
        Assert.assertEquals("¿Se cumplió el tiempo?", getTextElement("¿Se cumplió el tiempo?"));
    }


    @Y("^Detalle alarma$")
    public void alarmDetail() throws Exception {
        Assert.assertEquals("Opciones", getTextElement("Opciones"));
    }

    @Y("^refresco sesión$")
    public void refreshSession() throws Exception {
        readPage("HomePage.json");
        clickOnElement("Icono cerrar sesión");
        clickOnElement("Refrescar sesión");
        readPage("HistoryAlarmsSCADAPage.json");
        waitForElementVisible("Fecha");
    }

    @Y("cierro sesión")
    public void closeSession() throws Exception {
        readPage("HomePage.json");
        clickOnElement("Icono cerrar sesión");
        clickOnElement("Cerrar sesión");
    }

    @Entonces("se realiza logout exitosamente")
    public void logOut() throws Exception {
        readPage("LoginPage.json");
        waitForElementVisible("Iniciar sesión");
        Assert.assertEquals("Iniciar sesión", getTextElement("Iniciar sesión"));
    }

    @Entonces("veo el dashboard de SCADA")
    public void veoElDashboardDeSCADA() throws Exception {
        readPage("LoginPage.json");
        waitForElementPresent("Home");
    }
}
