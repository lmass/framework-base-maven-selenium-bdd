package stepDefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.es.Dado;
import functions.BasePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class HomeStepDefinitions extends BasePage {
    WebDriver driver;

    /******** Log Attribute ********/
    Logger log = Logger.getLogger(StepDefinitions.class);


    public HomeStepDefinitions() throws Exception {
        driver = Hooks.driver;
        readPage("LoginPage.json");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    /******** Scenario Attributes ********/
    Scenario scenario = null;
    public void scenario (Scenario scenario) {
        this.scenario = scenario;
    }

    @Dado("^Dado que estoy autenticado y cuento con los permisos necesarios para ejecutar la operación")
    public void navigateAzimut() throws Exception {

        setEnvironment("producción"); //extract the credentials and environment info (username, pass, url)
        String url = ScenaryData.get("Url");
        String userName = ScenaryData.get("UserName");
        String password = ScenaryData.get("Password");

        log.info("Navigate to: " + url);
        driver.get(url);
        pageHasLoaded();

        sendText("Username", userName);
        sendText("Password", password);

        By SeleniumElement = getCompleteElement("Iniciar sesión");
        driver.findElement(SeleniumElement).click();
        log.info("Click on: Iniciar sesion");

    }
}
