# language: es
Característica: Histórico de alarmas de SCADA

  Rule: Conocer el histórico de los eventos de una alarma configurada

    Antecedentes:
      Dado Dado que estoy autenticado y cuento con los permisos necesarios para ejecutar la operación


    #CRITERIO 1
    Escenario: Como QA, quiero visualizar la opción para redireccionar al panel del histórico de alarmas
      Cuando Selecciono organización Tigo
      Entonces Tengo la opción en el panel lateral de ir a histórico de eventos de las alarmas

    #CRITERIO 2
    @regression
    Escenario: Como QA, quiero visualizar el histórico de eventos de las alarmas
      Cuando Selecciono organización Tigo
      Y doy click a Histórico Alarmas
      Entonces espero que direccione la página al histórico de alarmas con la siguiente información: Fecha
      Y Fecha cierre
      Y Acción
      Y Nombre alarma
      Y Prioridad
      Y Inmueble
      Y Tiempo para resolverlo
      Y Tiempo en el que se resolvió
      Y Se cumplió el tiempo
      Y Detalle alarma

    @regression
    Escenario: Como QA, quiero refrescar sesión desde el histórico de alarmas
      Cuando Selecciono organización Tigo
      Y doy click a Histórico Alarmas
      Y refresco sesión
      Entonces espero que direccione la página al histórico de alarmas con la siguiente información: Fecha
      Y Fecha cierre
      Y Acción
      Y Nombre alarma
      Y Prioridad
      Y Inmueble
      Y Tiempo para resolverlo
      Y Tiempo en el que se resolvió
      Y Se cumplió el tiempo
      Y Detalle alarma

    @regression
    Escenario: Como QA, quiero cerrar sesión desde el histórico de alarmas
      Cuando Selecciono organización Tigo
      Y doy click a Histórico Alarmas
      Y cierro sesión
      Entonces se realiza logout exitosamente

    @regression
    Escenario: Como QA, quiero cambiar de organización desde el histórico de alarmas
      Cuando Selecciono organización Tigo
      Y doy click a Histórico Alarmas
      Y cambio de organización a: Pactia
      Entonces veo el dashboard de SCADA