Feature: Servicio de gateways.
  Rule: Como usuario podré hacer un CRUD de este servicio

    Background:
      Given Dado que estoy autenticado y cuento con los permisos necesarios para ejecutar la operación

    @Gateway
    Scenario Outline: Como QA, quiero validar que un gateway esté presente en la lista de gateways
      When Selecciono organización <organization>
      And Voy a lista de gateways
      Then Obtengo lista de gateways creados
      And Encuentro el gateway: <gateway> en la lista de gateways
      Examples:
        |organization  |gateway|
        |Pruebas Azimut|AZIMUT: AZIPR|
        |Pruebas Azimut|Alacaravan Zona Comun 2|


    @Smoke @Regression
    Scenario: Como QA, quiero actualizar un nuevo gateway
      When Selecciono un gateway para editar
      And Edito información a actualizar
      Then El gateway actualizado se visualiza en la lista de gateways

    Scenario: Como QA, quiero crear un nuevo gateway
      When Creo un nuevo medidor
      Then El nuevo gateway creado se visualiza en la lista de gateways