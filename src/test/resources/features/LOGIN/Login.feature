Feature: Probar login de plataforma plataforma

  Scenario: Entro con credenciales correctas
    Given Ingreso a la página principal de Azimut
    When Ingreso nombre de usuario válido
    And Ingreso contraseña correcta
    And Doy click en entrar
    Then Ingreso a lista de organizaciones y selecciono Pruebas Azimut
