# Azimut Framework E2E
url producción: https://app.azimutenergia.co/

## Librerías y frameworks empleados
* Metodología: BDD
* Cucumber4
* Junit4
* Maven
* Logger: log4j
* Selenium
* Allure Framework
* Patrón de diseño: Page Object Model
* Lenguaje: java 15

## Correr tests

### Configuración inicial
    mvn install

### Borrar historial de tests
    mvn clean

### Correr todos los tests
    mvn clen verify
    mvn verify

### Correr con tags
    mvn verify -Dcucumber.options="--tags @regression"

### Agregar informe Allure
    mvn mvn allure:report
    mvn allure:serve